package com.revolut.rates.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.revolut.rates.models.Rates;
import com.revolut.rates.models.RatesResponse;
import com.revolut.rates.models.retrofit.ErrorResponse;
import com.revolut.rates.models.retrofit.Result;
import com.revolut.rates.retrofit.ApiRequest;
import com.revolut.rates.retrofit.ErrorUtils;
import com.revolut.rates.retrofit.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatesRepository {

    public RatesRepository() {
    }

    public LiveData<Result<Rates[]>> getRates(String currency) {
        MutableLiveData<Result<Rates[]>> result = new MutableLiveData<>();

        ServiceGenerator.getApi().getRates(currency).enqueue(new Callback<RatesResponse>() {
            @Override
            public void onResponse(Call<RatesResponse> call, Response<RatesResponse> response) {

                if (response.isSuccessful()) {
                    Rates rates = response.body().getRates();
                    result.setValue(new Result.Success<>(rates));
                } else {
                    ErrorResponse errorResponse = ErrorUtils.parseError(response);
                    result.setValue(new Result.Error(errorResponse.getError()));
                }
            }

            @Override
            public void onFailure(Call<RatesResponse> call, Throwable t) {
                result.setValue(new Result.Error(t.getMessage()));
            }
        });
        return result;
    }

}
