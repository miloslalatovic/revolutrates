package com.revolut.rates.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.revolut.rates.R;
import com.revolut.rates.interfaces.OnCurrencyChangeListener;
import com.revolut.rates.models.Currency;
import com.revolut.rates.models.Rates;
import com.revolut.rates.models.retrofit.Result;
import com.revolut.rates.utils.Preferences;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements OnCurrencyChangeListener {

    private MainViewModel viewModel;
    private RecyclerView recyclerView;
    private CurrencyAdapter adapter = new CurrencyAdapter(this, this);
    private Handler handler = new Handler();
    private Snackbar snackbar;
    private String topCurrencyShortName = "AUD";

    private boolean isFirstRunDone;

    private final int refreshRate = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);

        recyclerView = findViewById(R.id.rates_recycler);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        Objects.requireNonNull(recyclerView.getItemAnimator()).setChangeDuration(0);

        snackbar = Snackbar.make(findViewById(android.R.id.content), getString(R.string.refresh_error), Snackbar.LENGTH_INDEFINITE);

        ArrayList<Currency> savedCurrencies = Preferences.getSavedCurrencies(this);
        if (savedCurrencies.size() > 0) {
            topCurrencyShortName = savedCurrencies.get(0).getKey();
            isFirstRunDone = true;
            adapter.setItems(savedCurrencies);
        } else {
            //ignore
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                getRates(topCurrencyShortName);
                handler.postDelayed(this, refreshRate);
            }
        };
        handler.post(runnableCode);
    }

    private void getRates(String currency) {

        viewModel.getRates(currency).observe(this, result -> {

            if (result instanceof Result.Success) {

                findViewById(R.id.rates_no_data).setVisibility(View.GONE);

                Rates rates = ((Result.Success<Rates>) result).getData();
                ArrayList<Currency> currencies = new ArrayList<>();

                Field[] fields = rates.getClass().getDeclaredFields();
                for (Field field : fields) {
                    try {
                        field.setAccessible(true);
                        if (field.get(rates) != null) {
                            currencies.add(new Currency(field.getName(), (Float) field.get(rates)));
                        } else if (!isFirstRunDone) {
                            currencies.add(new Currency(field.getName(), getInitialValue()));
                        } else {
                            //ignore
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                snackbar.dismiss();
                adapter.setItems(currencies);
            } else {
                snackbar.show();
            }
        });
    }

    private float getInitialValue() {
        isFirstRunDone = true;
        return 1f;
    }

    @Override
    public void onCurrencyClick(Currency currency) {
        runOnUiThread(() -> {
            topCurrencyShortName = currency.getKey();
            recyclerView.postDelayed(() -> recyclerView.smoothScrollToPosition(0), 300);
        });
    }
}
