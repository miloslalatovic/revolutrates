package com.revolut.rates.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.revolut.rates.models.Rates;
import com.revolut.rates.models.retrofit.Result;
import com.revolut.rates.repository.RatesRepository;

public class MainViewModel extends AndroidViewModel {

    private RatesRepository repository;

    public MainViewModel(@NonNull Application application) {
        super(application);
        repository = new RatesRepository();
    }

    LiveData<Result<Rates[]>> getRates(String currency) {
        return repository.getRates(currency);
    }
}