package com.revolut.rates.ui;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.revolut.rates.R;
import com.revolut.rates.interfaces.OnCurrencyChangeListener;
import com.revolut.rates.models.Currency;
import com.revolut.rates.utils.Preferences;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder> {

    private ArrayList<Currency> items = new ArrayList<>();
    private OnCurrencyChangeListener onCurrencyChangeListener;
    private Context context;

    CurrencyAdapter(Context context, OnCurrencyChangeListener onCurrencyChangeListener) {
        this.context = context;
        this.onCurrencyChangeListener = onCurrencyChangeListener;
    }

    void setItems(ArrayList<Currency> newItems) {

        if (items.size() > 0) {
            for (int i = 1; i < items.size(); i++) {
                for (Currency newItem : newItems) {
                    if (items.get(i).getKey().equals(newItem.getKey()) && newItem.getValue() != null) {
                        items.get(i).setValue(newItem.getValue());
                        notifyItemChanged(i);
                        break;
                    }
                }
            }
        } else {
            items = newItems;
            notifyDataSetChanged();
        }
        Preferences.saveCurrencies(context, items);
    }

    @Override
    public CurrencyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_currency, parent, false);
        return new CurrencyHolder(v);
    }

    @Override
    public void onViewRecycled(CurrencyHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onBindViewHolder(@NotNull CurrencyHolder holder, int position) {

        final Currency current = items.get(position);

        holder.itemView.setOnClickListener(view -> {
            if (holder.currencyValue.getText().toString().isEmpty() || holder.currencyValue.getText().toString().equals(".")) {
                current.setValue(0f);
            } else {
                current.setValue(Float.valueOf(holder.currencyValue.getText().toString()));
            }

            placeCurrencyOnTop(current, holder.getAdapterPosition());

            holder.currencyValue.requestFocus();
            InputMethodManager imm = (InputMethodManager) holder.itemView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.showSoftInput(holder.currencyValue, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        holder.currencyShortName.setText(current.getKey());

        Float value = current.getValue();
        if (value != null) {
            if (holder.getAdapterPosition() != 0) {
                value = items.get(0).getValue() * value;
            } else {
                //ignore
            }

            if (value.toString().endsWith(".0") || value.toString().endsWith(".")) {
                DecimalFormat df = new DecimalFormat("###");
                holder.currencyValue.setText(String.format("%s", df.format(value)));
            } else {
                holder.currencyValue.setText(String.format("%s", value));
            }
        } else {
            holder.currencyValue.setText("");
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (holder.currencyValue.isFocused()) {
                    if (s.toString().isEmpty() || s.toString().equals(".")) {
                        current.setValue(0f);
                    } else {
                        current.setValue(Float.valueOf(s.toString()));
                    }

                    for (int i = 1; i < getItemCount(); i++) {
                        notifyItemChanged(i);
                    }

                    Preferences.saveCurrencies(context, items);
                } else {
                    //ignore
                }
            }
        };

        holder.currencyValue.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus) {
                holder.currencyValue.addTextChangedListener(textWatcher);
                placeCurrencyOnTop(current, holder.getAdapterPosition());
            } else {
                holder.currencyValue.removeTextChangedListener(textWatcher);
            }
        });
    }

    private void placeCurrencyOnTop(Currency currency, int position) {
        if (position > 0) {
            items.remove(position);
            items.add(0, currency);
            notifyItemMoved(position, 0);

            onCurrencyChangeListener.onCurrencyClick(currency);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class CurrencyHolder extends RecyclerView.ViewHolder {

        private ImageView currencyCountryImg;
        private AppCompatTextView currencyShortName, currencyFullName;
        private AppCompatEditText currencyValue;

        private CurrencyHolder(View itemView) {
            super(itemView);
            currencyCountryImg = itemView.findViewById(R.id.currency_country_img);
            currencyFullName = itemView.findViewById(R.id.currency_full_name);
            currencyShortName = itemView.findViewById(R.id.currency_short_name);
            currencyValue = itemView.findViewById(R.id.currency_value);
        }
    }
}
