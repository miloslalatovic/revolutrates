package com.revolut.rates.interfaces;


import com.revolut.rates.models.Currency;

public interface OnCurrencyChangeListener {

    void onCurrencyClick(Currency currency);

}