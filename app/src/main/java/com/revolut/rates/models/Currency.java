package com.revolut.rates.models;

import androidx.annotation.NonNull;
import org.jetbrains.annotations.NotNull;

public class Currency {

    @NonNull
    private String key;
    private Float value;
    private int img;
    private String name;

    public Currency(@NotNull String key, Float value) {
        this.key = key;
        this.value = value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Float getValue() {
        return value;
    }

    public int getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public void setName(String name) {
        this.name = name;
    }
}
