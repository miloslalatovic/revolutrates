package com.revolut.rates.models;

public class RatesResponse {

    private String base;
    private String date;
    private Rates rates;

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public Rates getRates() {
        return rates;
    }
}
