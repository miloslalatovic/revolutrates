package com.revolut.rates.models;

import com.google.gson.annotations.SerializedName;

public class Rates {

    @SerializedName("AUD")
    private Float aud;
    @SerializedName("BGN")
    private Float bgn;
    @SerializedName("BRL")
    private Float brl;
    @SerializedName("CAD")
    private Float cad;
    @SerializedName("CHF")
    private Float chf;
    @SerializedName("CNY")
    private Float cny;
    @SerializedName("CZK")
    private Float czk;
    @SerializedName("DKK")
    private Float dkk;
    @SerializedName("GBP")
    private Float gbp;
    @SerializedName("HKD")
    private Float hkd;
    @SerializedName("HRK")
    private Float hrk;
    @SerializedName("HUF")
    private Float huf;
    @SerializedName("IDR")
    private Float idr;
    @SerializedName("ILS")
    private Float ils;
    @SerializedName("INR")
    private Float inr;
    @SerializedName("ISK")
    private Float isk;
    @SerializedName("JPY")
    private Float jpy;
    @SerializedName("KRW")
    private Float krw;
    @SerializedName("MXN")
    private Float mxn;
    @SerializedName("MYR")
    private Float myr;
    @SerializedName("NOK")
    private Float non;
    @SerializedName("NZD")
    private Float nzd;
    @SerializedName("PHP")
    private Float php;
    @SerializedName("PLN")
    private Float pln;
    @SerializedName("RON")
    private Float ron;
    @SerializedName("RUB")
    private Float rub;
    @SerializedName("SEK")
    private Float sek;
    @SerializedName("SGD")
    private Float sgd;
    @SerializedName("THB")
    private Float thb;
    @SerializedName("TRY")
    private Float tRy;
    @SerializedName("USD")
    private Float usd;
    @SerializedName("ZAR")
    private Float zar;
    @SerializedName("EUR")
    private Float eur;

}
