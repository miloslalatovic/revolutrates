package com.revolut.rates.retrofit;

import com.revolut.rates.models.RatesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiRequest {

    @GET("latest")
    Call<RatesResponse> getRates(@Query("base") String base);
}
