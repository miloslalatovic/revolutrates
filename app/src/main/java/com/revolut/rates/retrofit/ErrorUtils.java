package com.revolut.rates.retrofit;

import com.revolut.rates.models.retrofit.ErrorResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static ErrorResponse parseError(Response<?> response) {

        if(response.code() == 500) {
            return new ErrorResponse("Internal server error");
        }

        Converter<ResponseBody, ErrorResponse> converter =
                ServiceGenerator.getRetrofitInstance()
                        .responseBodyConverter(ErrorResponse.class, new Annotation[0]);

        ErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse(e.getMessage());
        }

        return error;
    }
}