package com.revolut.rates.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.revolut.rates.models.Currency;

import java.util.ArrayList;
import java.util.Arrays;

public class Preferences {

    private static final String PREFS = "REVOLUT_RATES_PREFS";
    private static final String RATES = "RATES";


    public static ArrayList<Currency> getSavedCurrencies(Context context) {
        final String empty = "EMPTY";
        String json = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE).getString(RATES, empty);
        if (json.equals(empty)) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(Arrays.asList(new Gson().fromJson(json, Currency[].class)));
        }
    }

    public static void saveCurrencies(Context context, ArrayList<Currency> currencies) {
        context.getSharedPreferences(PREFS, Context.MODE_PRIVATE).edit().putString(RATES, new Gson().toJson(currencies)).apply();
    }

    public static void clearPreferences(Context context) {
        context.getSharedPreferences(PREFS, Context.MODE_PRIVATE).edit().clear().apply();
    }

}
